﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazoncom
{
   static class Basket
    {
        static int[] _produstId = new int[20];
        static int _counter = 0;


        public static void addToBasket(int id)
        {
            _produstId[_counter] = id;
            _counter++;
        }
        
        public static int ItemsCountInBasket()
        {
            return _counter;
        }

        public static int[] ItemsInBasket()
        {
            int[] ms = new int[_counter];
            for (int i = 0; i < _counter; i++)
            {
                ms[i] = _produstId[i];
            }
            return ms;
        }

        static BasketProducts[] _basketProducts = new BasketProducts[30];
        static int Bcounter = 0;

        public static BasketProducts HasInBasket( int id)
    {
        foreach (BasketProducts item in _basketProducts) 
        {
            if (item != null && item.Product.Id == id)
            {
                return item;
            }
        }
        return null;
    }

    public static void AddToBasket(BasketProducts basketProducts)
    {
            BasketProducts product = HasInBasket(basketProducts.Product.Id);
            if (product != null)
            {
                product.Count++;
                product.TotalSum = product.Count * product.Product.Price;
            }
            else
            {
                _basketProducts[Bcounter] = basketProducts;
                _basketProducts[Bcounter].Count = 1;
                _basketProducts[Bcounter].TotalSum = basketProducts.Product.Id;
                Bcounter++;


            }
        }

        public static BasketProducts[] GetAllBasketProducts()
    {
            return _basketProducts;
    }
    }
}
