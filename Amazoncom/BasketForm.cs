﻿using Amazoncom.UserControls;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amazoncom
{
    public partial class BasketForm : Form
    {
        public BasketForm()
        {
            InitializeComponent();
        }

        private void BasketForm_Load(object sender, EventArgs e)
        {
            int[] items = Basket.ItemsInBasket();
            BasketProducts[] products = Products.GetProducts(items);
            int y = 40;
            decimal summ = 0;
            foreach (BasketProducts product in products)
            {

                if (product != null)
                {
                BasketLineControl lineControl = new BasketLineControl();
                lineControl.lbl_productName.Text = product.Product.Name;
                lineControl.lbl_productSum.Text = product.Product.Price.ToString();
                lineControl.lbl_productCount.Text = product.Count.ToString();
                lineControl.lbl_productTotalSum.Text = product.TotalSum.ToString();
                lineControl.Location = new Point(30, y);
                summ += product.TotalSum;
                lineControl.btn_add.Click += Btn_add_Click;
                lineControl.btn_remove.Click += Btn_remove_Click;   
                Controls.Add(lineControl);
                y += 120;
                }
            }
            lbl_total.Text = summ.ToString();
        }

        private void Btn_remove_Click(object sender, EventArgs e)
        {
            BasketLineControl lineControl = (sender as Button).Parent as BasketLineControl;
            decimal itemCount = (decimal.Parse(lineControl.lbl_productCount.Text) - 1);
            decimal sum = decimal.Parse(lineControl.lbl_productSum.Text);
            if (itemCount == 0 )
            {
                lineControl.Parent.Controls.Remove(lineControl);
            }
            else
            {
            lineControl.lbl_productCount.Text = itemCount.ToString();
           
            lineControl.lbl_productTotalSum.Text = (itemCount * sum).ToString();
            }
            ModifyTotal(sum, '-');

        }
        private void ModifyTotal( decimal price, char symbol)
        {
            decimal TotalPrice = decimal.Parse(lbl_total.Text);
            if (symbol == '+')
            {
                lbl_total.Text = (TotalPrice + price).ToString();
            }
            else
            {
                lbl_total.Text = (TotalPrice - price).ToString();
            }
        }

        private void Btn_add_Click(object sender, EventArgs e)
        {
            BasketLineControl lineControl = (sender as Button).Parent as BasketLineControl;
            decimal itemCount = (decimal.Parse(lineControl.lbl_productCount.Text) + 1);
            lineControl.lbl_productCount.Text = itemCount.ToString();
            decimal sum = decimal.Parse(lineControl.lbl_productSum.Text);
            lineControl.lbl_productTotalSum.Text = (itemCount * sum).ToString();
            ModifyTotal(sum, '+');
        }
    }
}
