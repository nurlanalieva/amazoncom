﻿namespace Amazoncom
{
    partial class LoginForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbx_Email = new System.Windows.Forms.TextBox();
            this.txbx_Password = new System.Windows.Forms.MaskedTextBox();
            this.label_Email = new System.Windows.Forms.Label();
            this.label_Password = new System.Windows.Forms.Label();
            this.btn_login = new System.Windows.Forms.Button();
            this.lbl_errorMessageEmail = new System.Windows.Forms.Label();
            this.lbl_errorMessage = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txbx_Email
            // 
            this.txbx_Email.Location = new System.Drawing.Point(172, 160);
            this.txbx_Email.Margin = new System.Windows.Forms.Padding(6);
            this.txbx_Email.Name = "txbx_Email";
            this.txbx_Email.Size = new System.Drawing.Size(332, 31);
            this.txbx_Email.TabIndex = 0;
            this.txbx_Email.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txbx_Password
            // 
            this.txbx_Password.Location = new System.Drawing.Point(171, 330);
            this.txbx_Password.Margin = new System.Windows.Forms.Padding(6);
            this.txbx_Password.Name = "txbx_Password";
            this.txbx_Password.Size = new System.Drawing.Size(332, 31);
            this.txbx_Password.TabIndex = 1;
            // 
            // label_Email
            // 
            this.label_Email.AutoSize = true;
            this.label_Email.Location = new System.Drawing.Point(166, 130);
            this.label_Email.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label_Email.Name = "label_Email";
            this.label_Email.Size = new System.Drawing.Size(65, 25);
            this.label_Email.TabIndex = 2;
            this.label_Email.Text = "Email";
            this.label_Email.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_Password
            // 
            this.label_Password.AutoSize = true;
            this.label_Password.Location = new System.Drawing.Point(167, 283);
            this.label_Password.Margin = new System.Windows.Forms.Padding(6, 0, 6, 0);
            this.label_Password.Name = "label_Password";
            this.label_Password.Size = new System.Drawing.Size(106, 25);
            this.label_Password.TabIndex = 3;
            this.label_Password.Text = "Password";
            // 
            // btn_login
            // 
            this.btn_login.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btn_login.Location = new System.Drawing.Point(354, 480);
            this.btn_login.Margin = new System.Windows.Forms.Padding(6);
            this.btn_login.Name = "btn_login";
            this.btn_login.Size = new System.Drawing.Size(150, 44);
            this.btn_login.TabIndex = 4;
            this.btn_login.Text = "Login";
            this.btn_login.UseVisualStyleBackColor = false;
            this.btn_login.Click += new System.EventHandler(this.btn_login_Click);
            // 
            // lbl_errorMessageEmail
            // 
            this.lbl_errorMessageEmail.AutoSize = true;
            this.lbl_errorMessageEmail.ForeColor = System.Drawing.Color.Red;
            this.lbl_errorMessageEmail.Location = new System.Drawing.Point(167, 197);
            this.lbl_errorMessageEmail.Name = "lbl_errorMessageEmail";
            this.lbl_errorMessageEmail.Size = new System.Drawing.Size(0, 25);
            this.lbl_errorMessageEmail.TabIndex = 5;
            this.lbl_errorMessageEmail.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // lbl_errorMessage
            // 
            this.lbl_errorMessage.AutoSize = true;
            this.lbl_errorMessage.ForeColor = System.Drawing.Color.Red;
            this.lbl_errorMessage.Location = new System.Drawing.Point(167, 367);
            this.lbl_errorMessage.Name = "lbl_errorMessage";
            this.lbl_errorMessage.Size = new System.Drawing.Size(0, 25);
            this.lbl_errorMessage.TabIndex = 6;
            // 
            // LoginForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(764, 669);
            this.Controls.Add(this.lbl_errorMessage);
            this.Controls.Add(this.lbl_errorMessageEmail);
            this.Controls.Add(this.btn_login);
            this.Controls.Add(this.label_Password);
            this.Controls.Add(this.label_Email);
            this.Controls.Add(this.txbx_Password);
            this.Controls.Add(this.txbx_Email);
            this.ForeColor = System.Drawing.SystemColors.Highlight;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "LoginForm";
            this.Text = "Login Form";
            this.Load += new System.EventHandler(this.LoginForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txbx_Email;
        private System.Windows.Forms.MaskedTextBox txbx_Password;
        private System.Windows.Forms.Label label_Email;
        private System.Windows.Forms.Label label_Password;
        private System.Windows.Forms.Button btn_login;
        private System.Windows.Forms.Label lbl_errorMessageEmail;
        private System.Windows.Forms.Label lbl_errorMessage;
    }
}

