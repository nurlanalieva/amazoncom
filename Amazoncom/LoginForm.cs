﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amazoncom
{
    public partial class LoginForm : Form
    {
        public LoginForm()
        {
            InitializeComponent();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            bool isValidationOk = true;
            lbl_errorMessage.Text = "";
            lbl_errorMessageEmail.Text = "";


            if (String.IsNullOrWhiteSpace(txbx_Password.Text) || txbx_Password.Text.Length <= 6)
            {
               isValidationOk = false;
                lbl_errorMessage.Text = "Password can't be empty and Length \n must be greater than 6 or equeal";
            }
            if (String.IsNullOrWhiteSpace(txbx_Email.Text) || txbx_Email.Text.Length < 6 || !txbx_Email.Text.Contains("@"))
            {
               isValidationOk = false;
                lbl_errorMessageEmail.Text = "Email can't be empty and Length \n must be greater than 6 or equeal \n or should be correct email";
            }

            if (isValidationOk)
            {
                bool hasGivenUser = Users.findUser(txbx_Email.Text, txbx_Password.Text);

                if (hasGivenUser)
                {
                MessageBox.Show("Login succesful completed");
                }
                else
                {
                    MessageBox.Show("User doesn't exist");
                }
            }
        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void LoginForm_Load(object sender, EventArgs e)
        {

        }
    }
}
