﻿using Amazoncom.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amazoncom
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

     //   private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
      //  {

      //  }

        private void  SelectImage(int i, PictureBox pictureBox1)
        {
            Product[] products = Products.GetProducts();
            pictureBox1.Image = products[i - 1].Photo;
        }


        private void MainForm_Load(object sender, EventArgs e)
        {
            int x = 47;
            int y = 39;
            int nameY = 220;
            int priceY = 250;
            Product[] products = Products.GetProducts();

            for (int i = 1; i <= products.Length; i++)
            {
                if (i % 4 == 0)
                {
                    x = 47;
                    y += 240;
                    nameY += nameY + 30;
                    priceY += 250;

                }

            PictureBox pictureBox1 = new PictureBox();
            SelectImage(i, pictureBox1);
            pictureBox1.Image = Resources.phone1;
            pictureBox1.Location = new Point(x,y);
            pictureBox1.Size = new Size(154,174);
            pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

                Label labelName = new Label();
                labelName.Text = "Name : ";
                labelName.Text += products[i - 1].Name;
                labelName.Location = new Point(x, nameY);

                Label labelPrice = new Label();
                labelPrice.Text = "Price : ";
                labelPrice.Text += products[i-1].Price.ToString();
                labelPrice.Width = 60;
                labelPrice.Location = new Point(x, priceY);

                Button button = new Button();
                button.Text = "Add";
                button.Location = new Point(x + 65, priceY - 6);
                button.BackColor = Color.LightSkyBlue;
                button.ForeColor = Color.White;
                button.Cursor = Cursors.Hand;
                button.Click += Button_Click;
                button.Tag = products[i-1].Id;
                //bir nece kontrolu bir yerd elave etmek ucun control range den istifade edtmek olar yazilishi eledir

                //Controls.AddRange(new Control[] { pictureBox1, labelPrice, labelName });

                Controls.Add(pictureBox1);
                Controls.Add(labelName);
                Controls.Add(labelPrice);
                Controls.Add(button);


                x += 240;
            }


        }

        private void Button_Click(object sender, EventArgs e)
        {

            //c# da as komandasi bir referans typeden bashqa bir referance type kecmek ucun istifade olunur

            // bu halda ise cevrilme ugursuz olarsa extheption bas verecek /
            //sertifikat imtahanina dushur bu sual
          // belede yazmaq olar  Button btn = (Button)sender;

            //bu raiantin ustunluyu odurki eger geriye cevrilme  olmassa geriye null qaytaracaq
            Button btn = sender as Button;
            Basket.addToBasket(int.Parse(btn.Tag.ToString()));
            //bele yazmaq olar amma face dir sadece bir vahid artiri bize basketde elave etmesi lazimdi ona gore ahsagidaki kimi yazacagiq
            //  lbl_basket_items_count.Text = (int.Parse(lbl_basket_items_count.Text) + 1).ToString();
            lbl_basket_items_count.Text = Basket.ItemsCountInBasket().ToString();

        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            BasketForm basketForm = new BasketForm();
            basketForm.ShowDialog();
        }
    }
}
