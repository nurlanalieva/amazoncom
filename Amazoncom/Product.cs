﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazoncom
{
    class Product
    {
        public string Name;
        public decimal Price;
        public Image Photo;
        public int Id;
    }
}
