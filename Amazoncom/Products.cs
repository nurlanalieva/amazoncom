﻿using Amazoncom.Properties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazoncom
{
    class Products
    {
        private static Product[] _products = new Product[6];

        public static BasketProducts[] GetProducts(int[] items)
        {
            for (int i = 0; i < items.Length; i++)
            {
                Product product = GetproductById(items[i]);
                BasketProducts basketproduct = new BasketProducts();
                basketproduct.Product = product;
                Basket.AddToBasket(basketproduct);
              
            }
            return Basket.GetAllBasketProducts();
        }
        public static Product GetproductById(int id)
        {
            Product[] products = Products.GetProducts();
            foreach (Product item in products)
            {
                if (item.Id == id)
                {
                    return item;
                }
            }
            return null;
        }
        public static Product[] GetProducts()
        {
            _products[0] = new Product();
            _products[0].Name = "Iphone X";
            _products[0].Photo = Resources.phone1;
            _products[0].Price = 450;
            _products[0].Id = 1;

            _products[1] = new Product();
            _products[1].Name = "Iphone 7";
            _products[1].Photo = Resources.phone2;
            _products[1].Price = 500;
            _products[1].Id = 2;


            _products[2] = new Product();
            _products[2].Name = "Iphone 7s";
            _products[2].Photo = Resources.phone3;
            _products[2].Price = 400;
            _products[2].Id = 3;


            _products[3] = new Product();
            _products[3].Name = "Iphone Xs";
            _products[3].Photo = Resources.phone4;
            _products[3].Price = 1400;
            _products[3].Id = 4;


            _products[4] = new Product();
            _products[4].Name = "Iphone 5s";
            _products[4].Photo = Resources.phone5;
            _products[4].Price = 300;
            _products[4].Id = 5;


            _products[5] = new Product();
            _products[5].Name = "Iphone 8s";
            _products[5].Photo = Resources.phone6;
            _products[5].Price = 800;
            _products[0].Id = 5;


            return _products;
        }
      
    }
}
