﻿namespace Amazoncom
{
    partial class RegisterForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txbx_Name = new System.Windows.Forms.Label();
            this.txbx_Email = new System.Windows.Forms.Label();
            this.label_Password = new System.Windows.Forms.Label();
            this.label_ConfirmPassword = new System.Windows.Forms.Label();
            this.txbx_UserName = new System.Windows.Forms.TextBox();
            this.txbx_EmailRegister = new System.Windows.Forms.TextBox();
            this.txbx_Password = new System.Windows.Forms.TextBox();
            this.txbx_ConfirmPassword = new System.Windows.Forms.TextBox();
            this.btn_Register = new System.Windows.Forms.Button();
            this.label_NameError = new System.Windows.Forms.Label();
            this.label_EmailError = new System.Windows.Forms.Label();
            this.labelPasswordError = new System.Windows.Forms.Label();
            this.labelConfirmPasswordError = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txbx_Name
            // 
            this.txbx_Name.AutoSize = true;
            this.txbx_Name.Location = new System.Drawing.Point(191, 105);
            this.txbx_Name.Name = "txbx_Name";
            this.txbx_Name.Size = new System.Drawing.Size(68, 25);
            this.txbx_Name.TabIndex = 0;
            this.txbx_Name.Text = "Name";
            // 
            // txbx_Email
            // 
            this.txbx_Email.AutoSize = true;
            this.txbx_Email.Location = new System.Drawing.Point(191, 226);
            this.txbx_Email.Name = "txbx_Email";
            this.txbx_Email.Size = new System.Drawing.Size(65, 25);
            this.txbx_Email.TabIndex = 1;
            this.txbx_Email.Text = "Email";
            // 
            // label_Password
            // 
            this.label_Password.AutoSize = true;
            this.label_Password.Location = new System.Drawing.Point(191, 359);
            this.label_Password.Name = "label_Password";
            this.label_Password.Size = new System.Drawing.Size(106, 25);
            this.label_Password.TabIndex = 2;
            this.label_Password.Text = "Password";
            // 
            // label_ConfirmPassword
            // 
            this.label_ConfirmPassword.AutoSize = true;
            this.label_ConfirmPassword.Location = new System.Drawing.Point(191, 492);
            this.label_ConfirmPassword.Name = "label_ConfirmPassword";
            this.label_ConfirmPassword.Size = new System.Drawing.Size(186, 25);
            this.label_ConfirmPassword.TabIndex = 3;
            this.label_ConfirmPassword.Text = "Confirm Password";
            // 
            // txbx_UserName
            // 
            this.txbx_UserName.Location = new System.Drawing.Point(196, 133);
            this.txbx_UserName.Name = "txbx_UserName";
            this.txbx_UserName.Size = new System.Drawing.Size(310, 31);
            this.txbx_UserName.TabIndex = 4;
            // 
            // txbx_EmailRegister
            // 
            this.txbx_EmailRegister.Location = new System.Drawing.Point(196, 254);
            this.txbx_EmailRegister.Name = "txbx_EmailRegister";
            this.txbx_EmailRegister.Size = new System.Drawing.Size(310, 31);
            this.txbx_EmailRegister.TabIndex = 5;
            // 
            // txbx_Password
            // 
            this.txbx_Password.Location = new System.Drawing.Point(196, 387);
            this.txbx_Password.Name = "txbx_Password";
            this.txbx_Password.Size = new System.Drawing.Size(310, 31);
            this.txbx_Password.TabIndex = 6;
            // 
            // txbx_ConfirmPassword
            // 
            this.txbx_ConfirmPassword.Location = new System.Drawing.Point(196, 520);
            this.txbx_ConfirmPassword.Name = "txbx_ConfirmPassword";
            this.txbx_ConfirmPassword.Size = new System.Drawing.Size(310, 31);
            this.txbx_ConfirmPassword.TabIndex = 7;
            // 
            // btn_Register
            // 
            this.btn_Register.Location = new System.Drawing.Point(375, 627);
            this.btn_Register.Name = "btn_Register";
            this.btn_Register.Size = new System.Drawing.Size(131, 46);
            this.btn_Register.TabIndex = 8;
            this.btn_Register.Text = "Register";
            this.btn_Register.UseVisualStyleBackColor = true;
            this.btn_Register.Click += new System.EventHandler(this.btn_Register_Click);
            // 
            // label_NameError
            // 
            this.label_NameError.AutoSize = true;
            this.label_NameError.ForeColor = System.Drawing.Color.Red;
            this.label_NameError.Location = new System.Drawing.Point(191, 180);
            this.label_NameError.Name = "label_NameError";
            this.label_NameError.Size = new System.Drawing.Size(0, 25);
            this.label_NameError.TabIndex = 9;
            // 
            // label_EmailError
            // 
            this.label_EmailError.AutoSize = true;
            this.label_EmailError.ForeColor = System.Drawing.Color.Red;
            this.label_EmailError.Location = new System.Drawing.Point(191, 298);
            this.label_EmailError.Name = "label_EmailError";
            this.label_EmailError.Size = new System.Drawing.Size(0, 25);
            this.label_EmailError.TabIndex = 10;
            this.label_EmailError.Click += new System.EventHandler(this.label_EmailError_Click);
            // 
            // labelPasswordError
            // 
            this.labelPasswordError.AutoSize = true;
            this.labelPasswordError.ForeColor = System.Drawing.Color.Red;
            this.labelPasswordError.Location = new System.Drawing.Point(191, 430);
            this.labelPasswordError.Name = "labelPasswordError";
            this.labelPasswordError.Size = new System.Drawing.Size(0, 25);
            this.labelPasswordError.TabIndex = 11;
            this.labelPasswordError.Click += new System.EventHandler(this.labelPasswordError_Click);
            // 
            // labelConfirmPasswordError
            // 
            this.labelConfirmPasswordError.AutoSize = true;
            this.labelConfirmPasswordError.ForeColor = System.Drawing.Color.Red;
            this.labelConfirmPasswordError.Location = new System.Drawing.Point(191, 564);
            this.labelConfirmPasswordError.Name = "labelConfirmPasswordError";
            this.labelConfirmPasswordError.Size = new System.Drawing.Size(0, 25);
            this.labelConfirmPasswordError.TabIndex = 12;
            this.labelConfirmPasswordError.Click += new System.EventHandler(this.labelConfirmPasswordError_Click);
            // 
            // RegisterForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(12F, 25F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.ClientSize = new System.Drawing.Size(1035, 756);
            this.Controls.Add(this.labelConfirmPasswordError);
            this.Controls.Add(this.labelPasswordError);
            this.Controls.Add(this.label_EmailError);
            this.Controls.Add(this.label_NameError);
            this.Controls.Add(this.btn_Register);
            this.Controls.Add(this.txbx_ConfirmPassword);
            this.Controls.Add(this.txbx_Password);
            this.Controls.Add(this.txbx_EmailRegister);
            this.Controls.Add(this.txbx_UserName);
            this.Controls.Add(this.label_ConfirmPassword);
            this.Controls.Add(this.label_Password);
            this.Controls.Add(this.txbx_Email);
            this.Controls.Add(this.txbx_Name);
            this.ForeColor = System.Drawing.SystemColors.Highlight;
            this.Name = "RegisterForm";
            this.Text = "Register Form";
            this.Load += new System.EventHandler(this.RegisterForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label txbx_Name;
        private System.Windows.Forms.Label txbx_Email;
        private System.Windows.Forms.Label label_Password;
        private System.Windows.Forms.Label label_ConfirmPassword;
        private System.Windows.Forms.TextBox txbx_UserName;
        private System.Windows.Forms.TextBox txbx_EmailRegister;
        private System.Windows.Forms.TextBox txbx_Password;
        private System.Windows.Forms.TextBox txbx_ConfirmPassword;
        private System.Windows.Forms.Button btn_Register;
        private System.Windows.Forms.Label label_NameError;
        private System.Windows.Forms.Label label_EmailError;
        private System.Windows.Forms.Label labelPasswordError;
        private System.Windows.Forms.Label labelConfirmPasswordError;
    }
}