﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Amazoncom
{
    public partial class RegisterForm : Form
    {
        public RegisterForm()
        {
            InitializeComponent();
        }

        private void RegisterForm_Load(object sender, EventArgs e)
        {

        }

        private void btn_Register_Click(object sender, EventArgs e)
        {
            //name bosh olmamalidir , min 2 sinvol olmali
            //email bosh olmamalidir @isharesi olmalidir min 6 sinvol
            //password bosh olmammalidr min 6 sinvol olm
            //confirm password bosh olmamalidir passworda beraber olmalidir
            label_NameError.Text = "";
            label_EmailError.Text = "";
            labelPasswordError.Text = "";
            labelConfirmPasswordError.Text = "";

            bool isValidationOk = true;

            if (String.IsNullOrWhiteSpace(txbx_UserName.Text) || txbx_UserName.Text.Length <= 2)
            {
                isValidationOk = false;
                label_NameError.Text = "Name can't be empty and Length \n must be greater than 2 or equeal";
            }
            if (String.IsNullOrWhiteSpace(txbx_EmailRegister.Text) || txbx_EmailRegister.Text.Length < 6 || !txbx_EmailRegister.Text.Contains("@"))
            {
                isValidationOk = false;
                label_EmailError.Text = "Email can't be empty and Length \n must be greater than 6 or equeal \n or should be correct email";
            }
            if (String.IsNullOrWhiteSpace(txbx_Password.Text) || txbx_Password.Text.Length < 6)
            {
                isValidationOk = false;
                labelPasswordError.Text = "Password can't be empty and Length \n must be greater than 6 or equeal";
            }
            if (String.IsNullOrWhiteSpace(txbx_ConfirmPassword.Text) || txbx_Password.Text != txbx_ConfirmPassword.Text )
            {
                isValidationOk = false;
                labelConfirmPasswordError.Text = "Confirm Password can't be empty and Length \n must be greater than 6 or equeal";
            }

            if (isValidationOk)
            {
                this.Hide();
                LoginForm loginForm = new LoginForm();
                User user = new User();
                user.Email = txbx_EmailRegister.Text;
                user.Name = txbx_UserName.Text;
                user.Password = txbx_Password.Text;

                Users.Add(user);
                loginForm.ShowDialog();

            }
        }

        private void label_EmailError_Click(object sender, EventArgs e)
        {

        }

        private void labelPasswordError_Click(object sender, EventArgs e)
        {

        }

        private void labelConfirmPasswordError_Click(object sender, EventArgs e)
        {

        }
    }
}
