﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Amazoncom
{
    //statik klaslarin obyekti yaradilmir ondan cemi 1 denedir
    //statik c=klasin icinde her shy statik olmalidir
    //butun methodlar butun fieler
    //obyekt yaradilmir ve bu klasdan cemi bir denedir

    public static class Users
    {
        private static User[] _users = new User[10];
        private static int _counter = 0;

        public static void Add(User user)
        {
            _users[_counter] = user;
            _counter++;
        }
         public static User[] GetUsers()
        {
            return _users;
        }
        public static bool findUser(string email, string password)
        {
            bool isFound = false;
            for (int i = 0; i < _counter; i++)
            {
                if (_users[i].Email == email && _users[i].Password == password)
                {
                    isFound = true;
                    break;
                }
            }
            return isFound;
        }
    }
}
